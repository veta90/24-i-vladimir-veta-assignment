//We wont use a template, because we don't have a templating mechanisam. everything will be generated from here as inner html

define(function () {
    function generateView(webResultData) {
        var results = webResultData.items;
        if (results == undefined || results.length == 0)
            return "No results found";
        var returnString = "";
        results.forEach(function (entry) {
            returnString += '<a href="' + entry.image.contextLink + '" target="_blank"><img src="' + entry.link + '"/></a>';
        });
        returnString += "<br>";
        return returnString;
    }

    return {
        generateView: generateView
    };

});