//We wont use a template, because we don't have a templating mechanisam. everything will be generated from here as inner html

define(function () {
    function generateView(webResultData) {
        var results = webResultData.items;
        var cursor = undefined;
        // var cursor = webResultData.queries.nextPage.startIndex;
        if (results == undefined || results.length == 0)
            return "No results found";
        var returnString = "";
        results.forEach(function (entry) {
            returnString += "<b>" + entry.title + "</b><br>";
            returnString += '<a href="' + entry.link + '" target="_blank">' + entry.htmlSnippet + '</a><br>';
            returnString += entry.content + "<br>";
            returnString += "<br>";
        });
        returnString += "<br>";
        if (cursor === undefined)
            return returnString;
        returnString += '<div class="paginator">';
        cursor.forEach(function (entry) {
            returnString += ' <b> <a onclick="doSearch(true,' + entry.start + ')"> ' + entry.label + ' </a> </b> ';
        });
        returnString += '</div>';
        return returnString;
    }

    return {
        generateView: generateView
    };

});