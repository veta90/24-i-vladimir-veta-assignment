//we will just use global variables for on the fly searching
var lastSearched = "";
var currrentSearching = 0;

//override lock is if the button is pressed. This is needed because the service calls are not instant. page number is sent for paging.
function doSearch(ovverideLock, page) {
    var text = document.getElementById("txtSearch").value;
    //condition for not sending another request to google.
    if (!ovverideLock && (text == lastSearched || currrentSearching))
        return;
    var leftResultsContent = document.getElementById("leftResultsContent");
    var rightResultsContent = document.getElementById("rightResultsContent");
    if (text.length == 0) {
        leftResultsContent.innerHTML = "";
        rightResultsContent.innerHTML = "";
        return;
    }
    leftResultsContent.innerHTML = "Loading Results...";
    rightResultsContent.innerHTML = "Loading Results...";
    //lock it
    lastSearched = text;
    currrentSearching = 2;
    //send a request to google

    var urlWeb = "https://www.googleapis.com/customsearch/v1?q=" + encodeURI(text) + "&cx=005607402202424640494%3Amaaxcfvzzgw&key=AIzaSyDQEEkJBYrkR3cAaffYUPj9GG8NybueUJ0&num=5" + (page > 0 ? "&start=" + page * 5 : "");
    var client = new HttpClient();
    client.get(urlWeb, function (data) {
        //unlock
        currrentSearching--;
        require(['controllers/webSearchViewController'], function (webSearchViewController) {
            rightResultsContent.innerHTML = webSearchViewController.generateView(JSON.parse(data));
        });
    });

    var urlImages = "https://www.googleapis.com/customsearch/v1?q=" + encodeURI(text) + "&cx=005607402202424640494%3Amaaxcfvzzgw&key=AIzaSyDQEEkJBYrkR3cAaffYUPj9GG8NybueUJ0&num=5&searchType=image" + (page > 0 ? "&start=" + page * 5 : "");
    client.get(urlImages, function (data) {
        //unlock
        currrentSearching--;
        require(['controllers/imageSearchViewController'], function (imageSearchViewController) {
            leftResultsContent.innerHTML = imageSearchViewController.generateView(JSON.parse(data));
        });
    });

}


var HttpClient = function () {
    this.get = function (aUrl, aCallback) {
        var anHttpRequest = new XMLHttpRequest();
        anHttpRequest.onreadystatechange = function () {
            if (anHttpRequest.readyState == 4 && anHttpRequest.status == 200)
                aCallback(anHttpRequest.responseText);
        }

        anHttpRequest.open("GET", aUrl, true);
        anHttpRequest.send(null);
    }
}