//regular app object of phonegap
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    onDeviceReady: function() {
        console.log("Phonegap initialized");
    }
};

//we include dynamicly for each platform the core javascript for native integration
var mobile = navigator.userAgent.match(/Android|iPhone|iPad|iPod/i);
if (navigator.userAgent.match(/Android/i))
    document.write('<script type="text/javascript" src="appcore/andorid/phonegap.js"></script>');
if(navigator.userAgent.match(/iPhone|iPad|iPod/i))
    document.write('<script type="text/javascript" src="appcore/ios/phonegap.js"></script>');
if (mobile){
    document.write('<script type="text/javascript" src="appcore/ios/phonegap.js"></script>');
    app.initialize();
}
